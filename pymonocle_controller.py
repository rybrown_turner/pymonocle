#!/usr/bin/python3.4
"""Controller for interaction with the monocle api."""
import requests
import sys
from config.pymonocle_config import PyMonocleConfig


class PyMonocleController:
    """PyMonocle controller class."""
    def __init__(self, token):
        """Initialize the controller."""
        cfg = PyMonocleConfig()
        self.activate_url = (cfg.activate_url)
        self.deactivate_url = (cfg.deactivate_url)
        self.env = cfg.env
        self.token = token

    def activate(self, link_name, link_destination, is_publicly_accessible,
                 access_level, is_api=False):
        """Method to activate a site and return peer data."""
        """Sanity check variables."""
        if not all([link_name, link_destination,
                    is_publicly_accessible is not None,
                    access_level, self.token]):
            raise ValueError('link_name, link_destination, '
                             'is_publicly_accessible, access_level, '
                             'token are all required arguments. Your '
                             'inputs were: link_name=%s, link_destination=%s, '
                             'is_publicly_accessible=%s, access_level=%s, '
                             'token=%s' %
                             (link_name, link_destination,
                              is_publicly_accessible, access_level,
                              self.token))
        data = {'link_name': link_name,
                'link_destination': link_destination,
                'public_accessible': is_publicly_accessible,
                'environment': self.env['ENVIRONMENT'],
                'access_level': access_level, 'token': self.token,
                'is_api': is_api}
        try:
            response = requests.post(self.activate_url, data=data,
                                     verify=False)
            peer_dict = response.json()
            return peer_dict
        except requests.ConnectionError as err:
            print(err)
            print(err.reason)

    def deactivate(self, link_name, link_destination, is_publicly_accessible,
                   access_level):
        """Method to deactivate a site."""
        """Sanity check variables."""
        if not all([link_name, link_destination,
                    is_publicly_accessible is not None, access_level,
                    self.token]):
            raise ValueError('link_name, link_destination, '
                             'is_publicly_accessible, access_level, '
                             'token are all required arguments. Your '
                             'inputs were: link_name=%s, link_destination=%s, '
                             'is_publicly_accessible=%s, access_level=%s, '
                             'token=%s' %
                             (link_name, link_destination,
                              is_publicly_accessible, access_level,
                              self.token))
        post_data = {'link_name': link_name,
                     'link_destination': link_destination,
                     'public_accessible': is_publicly_accessible,
                     'environment': self.env['ENVIRONMENT'],
                     'access_level': access_level, 'token': self.token}
        try:
            response = requests.post(self.deactivate_url, data=post_data,
                                     verify=False)
            peer_dict = response.json()
            return peer_dict
        except requests.ConnectionError as err:
            print(err)


def main(argv):
    """The main method, aka - how to use this thing."""
    controller = PyMonocleController('a6db4a38038d8d0f85a42c82a3c40b37')
    print(controller.activate(link_name='testing123',
                              link_destination='http://test123url.com/',
                              is_publicly_accessible=False,
                              access_level='1024'))
    print(controller.deactivate(link_name='testing123',
                                link_destination='http://test123url.com/',
                                is_publicly_accessible=False,
                                access_level='1024'))


if __name__ == '__main__':
    try:
        main(sys.argv)
    except IndexError:
        print(IndexError)
